import { Book } from "../entities";

// Your service interface should have all the methods that your RESTful web service
// will find helpful.
// 1. Methods that perform CRUD operation
// 2. Bussiness Logic operations 
export default interface BookService{

    registerBook(book:Book):Promise<Book>;

    retrieveAllBooks():Promise<Book[]>;

    retrieveBookById(bookId:number):Promise<Book>;

    checkoutBookById(bookId:number):Promise<Book>;

    checkinBookById(bookId:number):Promise<Book>;

    searchByTitle(title:string):Promise<Book[]>

    modifyBook(book:Book):Promise<Book>;

    removeBookById(bookId:number):Promise<boolean>;

}