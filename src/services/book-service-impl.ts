import { BookDAO } from "../daos/book-dao";
import { BookDaoPostgres } from "../daos/book-dao-postgres";
import { BookDaoTextFile } from "../daos/book-dao-textfile-impl";
import { Book } from "../entities";
import BookService from "./book-service";

export class BookServiceImpl implements BookService{

    bookDAO:BookDAO = new BookDaoPostgres();

    registerBook(book: Book): Promise<Book> {
        book.returnDate = 0;// services are often sanitize inputs. or set default values
        if(book.quality <1){
            book.quality = 1;
        }
        return this.bookDAO.createBook(book);
    }

    retrieveAllBooks(): Promise<Book[]> {
        return this.bookDAO.getAllBooks();
    }

    retrieveBookById(bookId: number): Promise<Book> {
        return this.bookDAO.getBookById(bookId);
    }

    // service methods also perform bussiness logic
    async checkoutBookById(bookId: number): Promise<Book> {
        let book:Book = await this.bookDAO.getBookById(bookId);
        book.isAvailable = false;
        book.returnDate = (Date.now() + 1_209_600); // you can use underscores in number for readability
        book = await this.bookDAO.updateBook(book);
        return book;
    }


    checkinBookById(bookId: number): Promise<Book> {
        throw new Error("Method not implemented.");
    }


    searchByTitle(title: string): Promise<Book[]> {
        throw new Error("Method not implemented.");
    }


    modifyBook(book: Book): Promise<Book> {
        throw new Error("Method not implemented.");
    }


    removeBookById(bookId: number): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
    
}