import {Client} from 'pg';
require('dotenv').config({path:'C:\\Users\\AdamRanieri\\Desktop\\LibraryAPI\\app.env'})
// hello!!!!!
export const client = new Client({
    user:'postgres',
    password:process.env.DBPASSWORD, //YOU SHOULD NEVER STORE PASSWORDS IN CODE
    database:process.env.DATABASENAME,
    port:5432,
    host:'34.145.203.69'
});
client.connect()